package com.cursoci;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class CalculatorTest {

    @Test
    public void deveSomar() {
        int total = new Calculator().somar(2, 3);
        assertEquals(5, total);
    }
}
